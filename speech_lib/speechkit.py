#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 truong-d <truong-d@truongd-XPS13-9333>
#
# Distributed under terms of the MIT license.

"""
.. module:: speekit
   :platform: Unix, Windows

.. moduleauthor:: Truong Do <andrew@invalid.com>

"""
from threading import Thread
import time
from recorder import Recorder
import urllib
from connection import StatusServer, SpeechServer
import mylog
import sys

CONTENT_TYPE_RAW = "audio/x-raw, layout=(string)interleaved, rate=(int)%d, format=(string)S16LE, channels=(int)1" % (16000)
CONTENT_TYPE_SPEEX = "audio/ogg, layout=(string)interleaved, rate=(int)%d, format=(string)S16LE, channels=(int)1" % (16000)


class Recognizer (Thread):
    """Recognizer object that actually perform speech recognition by the following step:

    1. Take the chunk of recorded audio
    2. Send it to server
    3. Wait for the result asynchronously

    This object is usually initialized in :class:`speechkit.SpeechKit`.
    """

    def __init__(self, vad=None):
        self.logger = mylog.init(__name__)
        super(self.__class__, self).__init__()
        self._recorder = Recorder(callback=self.on_audio_data, vad=vad)
        self.is_stop = False   # True when stop recording
        self.is_done = False   # True when disconnect with server
        self.speech_sock = None
        self.listener = None

    def enable_speex(self):
        """
        Enable speex encoding.

        Currently under development. Please do not call this function.

        """
        self._recorder.enable_speex()

    def set_sock(self, speech_sock):
        self.speech_sock = speech_sock

    # Callback function
    def on_audio_data(self, data):
        """Callback function that received the audio from a recorder,
        and send it to server

        :param data: Speech audio chunk
        :type data: binary_list
        :returns: None

        After sending the audio to server, the response can be received in
        another callback function :attr:`speechkit.SpeechKit.listener`
        """

        if not data:
            self.logger.error("no audio")
            return
        try:
            if not self.speech_sock.is_close:
                self.speech_sock.send(data, binary=True)
            else:
                self._recorder.stop()
                self.stop()
        except:
            self.speech_sock.shutdown()

    def run(self):
        if not self.speech_sock:
            self.logger.error("The connection is broken, disconnected")
            return
        self._recorder.start_record()

        while not self.is_stop:
            a=1

        if not self._recorder.is_stop:
            self._recorder.stop()

        print "speech sock status", self.speech_sock.is_close
        if not self.speech_sock.is_close:
            self.speech_sock.send("EOS")
            self.logger.info("Send EOS")
        else:
            self.done()

        while True:
            if self.is_done:
                break
            else:
                time.sleep(1)

    def stop(self):
        """
        Stop recognizer
        """
        self.is_stop = True

    def done(self):
        self.is_done = True

    def set_listener(self, listener):
        """
        Set callback function that will be call when received ASR result from server
        """
        self.listener = listener


def log(*args, **kwargs):
    if kwargs["debug"]:
        print >> sys.stderr, args


class SpeechKit(Thread):
    """
    Interface for main program
    """
    # If fullurl is defined, then use it, and ignore ip + lang
    # The fullurl param is typically use in testing mode, or simple demo
    # since we already know which language, and also server address
    def __init__(self, ip="ws://163.221.94.22:11507/", lang="en_nnet",
                 fullurl=None, listener=None, debug=False, vad=None):
        super(self.__class__, self).__init__()
        self.logger = mylog.init(__name__)
        self.listener = listener
        self.vad = vad

        if fullurl:
            log("Using fullurl "+ fullurl, debug=debug)
            self.logger.info("Using fullurl %s", fullurl)
            self.fullurl = fullurl
        else:
            log("Initialized speechkit: Ip-"+ip+", lang: " + lang, debug=debug)
            self.logger.info("Initialized speechkit: Ip-%s, lang: %s", ip, lang)
            self.fullurl = ip + lang + "/speech-api/ws/"

        self.content_type = CONTENT_TYPE_RAW
        self.is_stop = False
        self.recognizer = None  # will be set in connect function

    def connect(self):
        """
        Build the connection
        """
        # Define speech and status server
        self.status_server = StatusServer(self.fullurl + "/status" +
                '?%s' % (urllib.urlencode([("content-type", self.content_type)])), self)
        self.speech_server = SpeechServer(self.fullurl + "/speech" +
                '?%s' % (urllib.urlencode([("content-type", self.content_type)])),
                self.listener, control_listener=self)

        # Define recorder, and recognizer service
        # TODO: make recorder support speex
        self.recognizer = Recognizer(vad=self.vad)
        self.recognizer.set_sock(self.speech_server)
        if self.content_type == CONTENT_TYPE_SPEEX:
            self.recognizer.enable_speex()

    def debug(self):
        print "Speechkit information:"
        print "URL: %s" % (self.fullurl)
        print "CONTENT_TYPE: %s" % (self.content_type)

    def set_content_type(self, content_type):
        """
        Set content type
        """
        self.content_type = content_type

    @classmethod
    def initialize(cls, ip="ws://163.221.94.22:11507/", lang="en_nnet", listener=None, fullurl=None, **kwargs):
        """ Initializing the speechkit. This should be called from main program
        in order to create a new Speechkit,

        :param ip: Server IP adress (in ``ws`` form)
        :type ip: str
        :param lang: Language. This can be *en_nnet*, *en*, *ja_nnet*, *ja_nnet_fnn*
        :type lang: str
        :param listener: Callback function, that will be called when got a result from ASR server
        :param fullurl: If this option is specified, then the SpeechKit will be initialized with fullurl, and discard *ip* + *lang* as well. Often use when you know the direct url of ASR worker. E.g. ws://localhost:8888/client/ws
        :param kwargs: Keyword argument. Use to specify option ``debug=True``

        Some examples how to use this method:

        >>> speech_kit = SpeechKit.initialize(ip="ws://163.221.94.22:11507/",
                                              lang="en_nnet", listener=result_listener, fullurl=None)
        Initializing with fullurl:
        >>> fullurl_en="ws://localhost:8124/client/ws/"
        >>> speech_kit = SpeechKit.initialize(listener=result_listener, fullurl=fullurl_en)

        """
        if not listener:
            print "listener is None, please define one"
            return None

        _speech_kit = cls(ip=ip, lang=lang, fullurl=fullurl, listener=listener, **kwargs)
        return _speech_kit

    # ======================= Define callback functions ====================
    # Starting to send the audio
    # when the speech server is connected
    def speech_server_connected(self):
        """ Callback function. This function will be called when the speech server is connected
        sucessfully with the ASR server. From here, we are ready to send audio data.

        """
        self.recognizer.start()

    # Called when speech server done
    def speech_server_done(self):
        """ Callback function. This function will be called when speech server is disconnected
        with ASR server. We will stop the :attr:`recognizer` from here.

        """
        self.recognizer.done()

    # Return number of worker available from server
    def has_worker(self, nworker):
        """ Callback function. This function will be called when status server
        receive data from ASR server say that we have :attr:`nworker` available.
        If the number of worker > 0, we will start :attr:`speech_server`

        """
        print "Server has", nworker, "workers"
        try:
            self.speech_server.connect()
        except:
            return
    # ======================= Done callback functions =====================

    def run(self):
        # Run status server, checking for server available
        self.status_server.connect()
        while not self.is_stop:
            time.sleep(1)
        self.status_server.close()

    def stop(self):
        # Shutdown speech kit
        # Workflow: 1. Stop recognizer, wait until it successfully stop
        #           2. set is_stop = True to turn shutdown speechkit
        if self.speech_server:
            self.recognizer.stop()

        while not self.recognizer.is_stop:
            time.sleep(1)
        self.is_stop = True

    def get_sock(self):
        return self.speech_server


class SimpleListener:
    def __init__(self):
        pass

    def on_partial(self, text):
        print text

    def on_final(self, text):
        print text

if __name__ == "__main__":
    # speech_kit = SpeechKit.initialize()
    speech_kit = SpeechKit.initialize(listener=SimpleListener(), debug=True)
    speech_kit.set_content_type(CONTENT_TYPE_RAW)
    speech_kit.connect()
    speech_kit.start()
    time.sleep(5)
    speech_kit.stop()
    while not speech_kit.is_stop:
        time.sleep(1)

#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 truong-d <truong-d@truongd-XPS13-9333>
#
# Distributed under terms of the MIT license.

"""

"""
import sys
import subprocess
import xmlrpclib

url = "http://iryo-server.naist.jp:11504/RPCSERV"
proxy = xmlrpclib.ServerProxy(url, encoding="utf-8")


def enja_translate(en_text):
    en_text = en_text.upper()
    p = subprocess.Popen(["echo \"%s\" | perl /home/truong-d/mt-client.pl -src en -trg ja" % (en_text)],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    return out.decode("utf-8").strip()


def jaen_translate(ja_text):
    p = subprocess.Popen(["echo \"%s\" | perl mt-client.pl -src ja -trg en" % (ja_text)],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    return out.decode("utf-8").strip()


def main(argv=sys.argv):
    print enja_translate("I WANT TO BY A NEW COMPUTER")
    #print jaen_translate("新しいパソコンジェーをしたいのですが")

if __name__ == '__main__':
    main()

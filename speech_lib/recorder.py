import pyaudio
try:
    import audiospeex
except:
    pass
from ctypes import *
from contextlib import contextmanager
import time
from struct import unpack

ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)

def py_error_handler(filename, line, function, err, fmt):
    pass

c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)


class Recorder:
    def __init__(self, rate=16000, chunk=1024, callback=None, vad=None):
        self.CHUNK = chunk
        self.FORMAT = pyaudio.paInt16
        self.CHANNELS = 1
        self.RATE = rate
        self.vad = vad
        self.p = pyaudio.PyAudio()
        self.callback = callback
        self.speex_enable = False
        self.is_stop = 0
        self.is_start_sending = False

    def enable_speex(self):
        self.speex_enable = True

    # TODO: fix speex encoding
    def on_audio_data(self, in_data, frame_count, time_info, status):
        enc = None
        fmt = "%ih" % frame_count
        integer_data = unpack(fmt, in_data)
        is_speech = 1
        if self.vad:
            is_speech = self.vad.decide(integer_data)
        if is_speech > 0.5 and (not self.is_start_sending):
            self.is_start_sending = True
            print "Got speech"

        if self.is_start_sending:
            try:
                if self.speex_enable:
                    speex_enc, enc = audiospeex.lin2speex(in_data, sample_rate=16000, state=enc)
                    self.callback(speex_enc)
                else:
                    self.callback(in_data)
            except AttributeError:
                self.logger.error("The connection is broken, disconnected")
                return
        return in_data, pyaudio.paContinue

    def start_record(self):
        self.stream = self.p.open(format=self.FORMAT,
                channels=self.CHANNELS,
                rate=self.RATE,
                input=True,
                frames_per_buffer=self.CHUNK,
                stream_callback=self.on_audio_data)
        self.stream.start_stream()

    def stop(self):
        self.is_stop = 1
        self.stream.stop_stream()
        self.stream.close()
        self.p.terminate()

    def is_recording(self):
        return self.stream.is_active()


if __name__ == "__main__":
    enc = None
    dec = None

    def callback(in_data, frame_count, time_info, status):
        global enc, dec
        fragment2, enc = audiospeex.lin2speex(in_data, sample_rate=16000, state=enc)
        fragment3, dec = audiospeex.speex2lin(fragment2, sample_rate=16000, state=dec)

        print len(in_data), len(fragment2), len(fragment3)
        print "state", enc, dec
        return in_data, pyaudio.paContinue

    recorder = Recorder(callback=callback)
    recorder.start_record()

    time.sleep(5)
    recorder.stop()

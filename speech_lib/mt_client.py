#!/usr/bin/env python
# -*- coding: utf-8 -*-

# python port of client.perl

import xmlrpclib
import optparse
from smartencoding import smart_unicode
import socket

parser = optparse.OptionParser()
parser.add_option("-v", action="store_true", dest="vis",default=False)

url = "http://ahcclm01.naist.jp:8080/RPC2"
url_graham = "http://iryo-server.naist.jp:11504/RPCSERV"
url_graham2 = "http://ahcclm04:30001/RPCSERV"
proxy = xmlrpclib.ServerProxy(url)
proxy_graham = xmlrpclib.ServerProxy(url_graham2, encoding="utf-8")
def en2ja_bygraham(en_text):
    #cmd=["echo", "\""+en_text+"\"", "| ./mt-client.pl","-src en -trg ja"]
    #p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #out,err = p.communicate()
    #return out
    result = proxy_graham.enja_translate(en_text)
    return result

def ja2en_bygraham(ja_text):
    #cmd=["echo", "\""+en_text+"\"", "| ./mt-client.pl","-src en -trg ja"]
    #p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #out,err = p.communicate()
    #return out
    result = proxy_graham.jaen_translate(ja_text)
    return result

def translate(en_text):
    params = {"text":en_text, "align":"true","word-align":"true", "report-all-factors":"true"}
    result = proxy.translate(params)
    ja_text = result['text']
    return ja_text

def en2ja_by_oda(en_text, host='ahcclm04', port=30000):
    bufsize = 4096
    try:
        text = en_text.encode('utf-8').lower()

        sock = socket.socket()
        sock.settimeout(5.0)
        sock.connect((host, port))
        sock.send(text)

        output = sock.recv(bufsize)

        output = output.decode('utf-8', 'replace')
        output = output.strip()
        return output

    except Exception as e:
        print e
        return en_text
        pass


if __name__ == '__main__':
    #print ja2en_bygraham("日本 サッカー 協会 は 今日 午後 体 の 理事 会 を 開き その 後 の 会見 で 今後 の 対応 に つい て 説明 を 行う")
    #print ja2en_bygraham("今日はどうなさいましたか")
    #print en2ja_bygraham("I WANT TO BY A NEW COMPUTER")
    # print smart_unicode(en2ja_bygraham("I want to by a new computer"))
    #print ja2en_bygraham("日本 サッカー 協会 は 今日 午後 体 の 理事 会 を 開き その 後 の 会見 で 今後 の 対応 に つい て 説明 を 行う")
    print(en2ja_by_oda('THIS IS A PEN\n'))
    print(en2ja_by_oda('i don\'t like him\n'))
    print(en2ja_by_oda('i do n\'t like him\n'))

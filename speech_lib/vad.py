#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 truong-d <truong-d@dellxps>
#
# Distributed under terms of the MIT license.

"""

"""

import math


class PowerVAD():
    """ This is implementation of a simple power based voice activity detector.

    It only implements simple decisions whether input frame is speech of non speech.
    """

    def __init__(self, threshold=None, adaptation_frames=0, threshold_multiplier=1.0):
        self.power_threshold_adapted = threshold
        self.adaptation_frames = adaptation_frames
        self.threshold_multiplier = threshold_multiplier
        self.in_frames = 0

    def decide(self, frame):
        """Returns whether the input segment is speech or non speech.

        The returned values can be in range from 0.0 to 1.0.
        It returns 1.0 for 100% speech segment and 0.0 for 100% non speech segment.
        """

        speech_segment = 0.0

        self.in_frames += 1

        a = [abs(x) ** 2 for x in frame]
        energy = math.sqrt(sum(a)) / len(a)

        if self.in_frames < self.adaptation_frames:
            self.power_threshold_adapted = self.in_frames * \
                self.power_threshold_adapted
            self.power_threshold_adapted += energy
            self.power_threshold_adapted /= self.in_frames + 1

        if energy > self.threshold_multiplier * self.power_threshold_adapted:
            speech_segment = 1.0

        return speech_segment

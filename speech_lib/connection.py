#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ws4py.client.threadedclient import WebSocketClient
import json
import sys


def log(*args):
    print >> sys.stderr, args


class StatusServer(WebSocketClient):

    def __init__(self, url, listener, protocols=None, extensions=None, heartbeat_freq=None, byterate=32000):
        super(StatusServer, self).__init__(url, protocols, extensions, heartbeat_freq)
        self.listener = listener
    def opened(self):
        pass

    def received_message(self, m):
        response = json.loads(str(m))
        if response.has_key("num_workers_available"):
            n_worker = int(response["num_workers_available"])
            global has_worker
            if n_worker > 0:
                self.listener.has_worker(n_worker)

    def closed(self, code, reason=None):
        pass
        log("DONE !")

class SpeechServer(WebSocketClient):

    def __init__(self, url, result_listener,control_listener,  protocols=None, extensions=None, heartbeat_freq=None, byterate=32000):
        super(self.__class__, self).__init__(url, protocols, extensions, heartbeat_freq)
        self.result_listener = result_listener
        self.control_listener = control_listener
        self.is_close = 0
    def opened(self):
        pass
        self.control_listener.speech_server_connected()

    def received_message(self, m):
        response = json.loads(str(m))

        if response.has_key("status"):
            if response['status'] == 0:
                # TODO: Fix this, when server return adaptation state, this raise error
                try:
                    trans = response['result']['hypotheses'][0]['transcript']
                except:
                    trans = ""

                if 'result' in response and response['result']['final']:
                    self.result_listener.on_final(trans)
                else:
                    self.result_listener.on_partial(trans)

    def shutdown(self):
        self.is_close = 1

    def closed(self, code, reason=None):
        self.is_close = 1
        self.control_listener.speech_server_done()


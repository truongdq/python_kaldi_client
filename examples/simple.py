#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Do Truong'

import sys
import os
import time
import re
from speech_lib.speechkit import SpeechKit


class Listener:
    def __init__(self):
        self.trans = []
        self.clean = re.compile("<UNK>\.|<unk>\.|<unk>|<UNK>|power.*")

    def on_partial(self, text):
        text = self.clean.sub("", text)
        if text:
            print " ".join(self.trans) + " " + text.lower()

    def on_final(self, text):
        text = self.clean.sub("", text)
        if text:
            self.trans.append(text)
            print " ".join(self.trans).upper()


def main():
    result_listener = Listener()

    # ===============  Please uncomment the lines below to use fullurl ============
    # fullurl_en="ws://localhost:8124/client/ws/"
    # fullurl_ja="ws://localhost:8123/client/ws/"
    # speech_kit = SpeechKit.initialize(listener=result_listener, fullurl=fullurl_en)
    # =============================================================================

    # Comment this out to use fullurl, otherwise, the speech_kit will
    # reinitialize to connect to extenal server
    speech_kit = SpeechKit.initialize(ip="ws://ahcclp01:11507/",
                                      lang="en_tedtalk", listener=result_listener, fullurl=None)

    if not speech_kit:
        print "Error, cannot initialize"
        exit(1)

    speech_kit.connect()
    speech_kit.start()

    # Do speech recognition in 5 second
    time.sleep(5)
    speech_kit.stop()

    # Have to wait until speech_kit shutdown successfully
    # Sometime the server is still recognizing the last few frames
    # so we have to wait until everything is shutdown
    while not speech_kit.is_stop:
        time.sleep(1)

if __name__ == '__main__':
    main()

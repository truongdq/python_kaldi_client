#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'tanel, modified by: Do Truong'

import sys
import os
import time
import speech_lib.myutils as myutils
from speech_lib.speechkit import SpeechKit
import speech_lib.mt_client as mt_client2
import speech_lib.vad as vad
import platform
import optparse
from threading import Thread
from multiprocessing import Lock


class Display:
    def __init__(self):
        pass

    def display(self, text):
        if platform.system() == "Linux":
            os.system('clear')
        else:
            os.system('cls')
        print text.encode("utf-8")
lock = Lock()


class Listener:
    def __init__(self):
        self.trans = []
        self.display = Display()
        self.do_translate = False
        import re
        self.clean = re.compile("<UNK>\.|<unk>\.|<unk>|<UNK>|power.*")

    def on_partial(self, text):
        text = self.clean.sub("", text)
        if text:
            self.display.display(" ".join([txt if ok else "<<" + txt + ">>"
                for ok, txt in self.trans]) + " " + text.lower())

    def perform_translate(self, text, idx):
        lock.acquire()
        translated = mt_client2.en2ja_by_oda(text.replace("n't", " n't").replace("n 't", " n't"))
        self.trans[idx][0] = 1
        self.trans[idx][1] = translated
        lock.release()

    def on_final(self, text):
        text = self.clean.sub("", text)
        if text:
            if self.do_translate:
                self.trans.append([0, text])
                idx = len(self.trans) - 1
                Thread(target=self.perform_translate, args=(text, idx, )).start()
            else:
                self.trans.append([1, text])

            self.display.display(" ".join([txt if ok else "<<" + txt + ">>" for ok, txt in self.trans]))


def log(*args):
    print >> sys.stderr, args

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option("-l", "--language", dest="lang", default="en_tedtalk",
                      help="Choose language (en_tedtalk|ja_webdemo|ja_nnet_fnn)")
    parser.add_option("-u", "--url", dest="url", default="master.ahclab.tk",
                      help="Nginx server ip address. E.g., master.ahclab.tk")
    opts, args = parser.parse_args()

    result_listener = Listener()

    # VAD
    # Threshold: start ASR when got a frame which has power >= 70.
    # adaptation_frames: Number of frames use to adapt the threshold.
    vad = vad.PowerVAD(threshold=70, adaptation_frames=2, threshold_multiplier=1.0)
    speech_kit = SpeechKit.initialize(ip="ws://" + opts.url + "/",
                                      lang=opts.lang, listener=result_listener, fullurl=None, vad=vad, debug=True)

    if not speech_kit:
        log("Can not initalize")
        exit(1)

    speech_kit.connect()
    speech_kit.start()

    getch = myutils._Getch()

    def keyFunc():
        while not speech_kit.is_stop:
            key = getch()
            if key == "t":
                if result_listener.do_translate:
                    log("Turn off translation")
                    result_listener.do_translate = False
                else:
                    log("Turn on translation")
                    result_listener.do_translate = True
            if key == "e":
                speech_kit.stop()
                break
            time.sleep(1)

    Thread(target=keyFunc())
    while not speech_kit.is_stop:
        time.sleep(1)

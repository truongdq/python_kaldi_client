#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 truong-d <truong-d@truongd-XPS13-9333>
#
# Distributed under terms of the MIT license.

"""

"""
import os
try:
    from setuptools import setup, Extension
except ImportError:
    from distutils.core import setup
__version__ = "1.3"

if 'SPEEX_INCLUDE' in os.environ:
    speex = Extension('audiospeex', sources=['audiospeex/audiospeex.cpp'],
                    include_dirs=[os.environ['SPEEX_INCLUDE']],
                    library_dirs=[os.environ['SPEEX_LIB']],
                    libraries=['speex', 'speexdsp'], extra_link_args=['-fPIC'])
    setup(name="speech_lib",
        version=__version__,
        author="Do Truong",
        author_email="do.q.truong@gmail.com",
        license="MIT",
        packages=['speech_lib'],
        keywords = "python kaldi gstreamer client",
        classifiers = ["Programming Language :: Python"],
        ext_modules=[speex]
    )
else:
    setup(name="speech_lib",
        version=__version__,
        author="Do Truong",
        author_email="do.q.truong@gmail.com",
        license="MIT",
        packages=['speech_lib'],
        keywords="python kaldi gstreamer client",
        classifiers=["Programming Language :: Python"]
    )
